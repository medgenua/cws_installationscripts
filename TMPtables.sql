-- MySQL dump 10.13  Distrib 5.1.39, for unknown-linux-gnu (x86_64)
--
-- Host: localhost    Database: CNVanalysis-TMP
-- ------------------------------------------------------
-- Server version	5.1.39-community-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `CNVanalysis-TMP`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `CNVanalysis-TMP` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `CNVanalysis-TMP`;

--
-- Table structure for table `BAFSEG`
--

DROP TABLE IF EXISTS `BAFSEG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BAFSEG` (
  `pid` bigint(20) NOT NULL,
  `sid` bigint(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `indications` text NOT NULL,
  PRIMARY KEY (`pid`,`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FamilyAnalysis`
--

DROP TABLE IF EXISTS `FamilyAnalysis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FamilyAnalysis` (
  `pid` bigint(20) NOT NULL,
  `sid` bigint(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`,`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Formatting`
--

DROP TABLE IF EXISTS `Formatting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Formatting` (
  `pid` bigint(20) NOT NULL,
  `sid` bigint(20) NOT NULL,
  `nrabs` int(11) NOT NULL,
  `multixml` longtext NOT NULL,
  `multitable` longtext NOT NULL,
  `qsnpxml` longtext NOT NULL,
  `pcnvxml` longtext NOT NULL,
  `vicexml` longtext NOT NULL,
  PRIMARY KEY (`pid`,`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Segmentation`
--

DROP TABLE IF EXISTS `Segmentation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Segmentation` (
  `pid` bigint(20) NOT NULL,
  `sid` bigint(20) NOT NULL,
  `algo` varchar(25) NOT NULL,
  PRIMARY KEY (`pid`,`sid`,`algo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UPD`
--

DROP TABLE IF EXISTS `UPD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UPD` (
  `pid` bigint(20) NOT NULL,
  `sid` bigint(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`,`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WholeGenomePlots`
--

DROP TABLE IF EXISTS `WholeGenomePlots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WholeGenomePlots` (
  `pid` bigint(20) NOT NULL,
  `sid` bigint(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`,`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-05-05 10:42:06
