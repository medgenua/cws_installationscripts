#!/bin/bash

#########################################
# GET SYSTEM DETAILS AND USER/PASSWORDS #
#########################################

if [[ $EUID -ne 0 ]]; then
  echo ""
  echo "###############################################################################"
  echo "# The installation scripts must be run as root user (sudo ./InstallScript.sh) #" 2>&1
  echo "###############################################################################"
  echo 
  exit 1
fi

clear 
echo ""
echo " ################"
echo " # Introduction #"
echo " ################"
echo ""
echo "  This installation script will install the CNV-WebStore Web Interface.  It will first install necessary packages from both the "
echo "  general repositories, CPAN an bioconductor. Then it will ask you to create a system user to run analysis scripts"
echo "  and a mysql user to access the results from the web interface. Most steps do not request interaction and will"
echo "  continue with default values after a few seconds. "
echo " "
echo "  IMPORTANT: Once you have entered the mysql-root password (to create the mysql user), do not interrupt the script, "
echo "  as (random) passwords have then be created and may conflict if the script is re-run. If installation or download of"
echo "  files fails before this step, you can safely abort (Ctrl-C) and retry installation. "
echo ""
read -p "  Hit enter to start the installation"  DUMMY

clear
## Where am I now?
INSTALLDIR=`pwd`
## change permissions on tmp, needs to be 777
#chmod 777 /var/tmp
## what system am I on?
echo ""
echo " ###########################"
echo " # PROBING FOR DISTRO TYPE #"
echo " ###########################"
if [ -e "/etc/debian_version" ]; then
	echo "  Detected Debian-Based Distro, installing software through APT"
	INSTALLCOMMAND='apt-get install -y'
	SEARCHCOMMAND='aptitude search'
	APACHERESTART='/etc/init.d/apache2 restart'
	DISTRO="debian"
elif [ -e "/etc/fedora-release" ]; then
	echo "  Detected Fedora-Based Distro, installing software through YUM"
	INSTALLCOMMAND='yum install -y'
	SEARCHCOMMAND='yum search'
	APACHERESTART='service httpd restart'
	DISTRO="fedora"
elif [ -e "/etc/gentoo-release" ]; then
	echo "  Detected Gentoo-Based Distro, installing software through EMERGE"
	INSTALLCOMMAND='emerge --newuse -u'
	SEARCHCOMMAND='emerge --search'
	DISTRO="gentoo"
elif [ -e "/etc/redhat-release" ]; then
	CONTENT=`cat "/etc/redhat-release"`
	if echo $CONTENT | grep -i "CentOS" ;then
		echo "  Detected CentOS Distro, installing softwere through YUM"
		INSTALLCOMMAND='yum install -y'
		SEARCHCOMMAND='yum search'
		APACHERESTART='service httpd restart'
		DISTRO="centos"
	else 
		echo "  Detected RedHat-Based Distro, installing softwere through YUM"
		INSTALLCOMMAND='yum install -y'
		SEARCHCOMMAND='yum search'
		APACHERESTART='service httpd restart'
		DISTRO="redhat"
	fi
elif [ -e "/etc/lsb-release" ]; then
	# generic
	CONTENT=`cat "/etc/lsb-release"`
	if echo $CONTENT | grep -i "ubuntu" ;then
		echo "  Detected Ubuntu Distro, installing softwere through APT"
		INSTALLCOMMAND='apt-get install -y'
		SEARCHCOMMAND='aptitude search'
		APACHERESTART='/etc/init.d/apache2 restart'
		DISTRO="ubuntu"
	else 
		echo "  Unknown distro type. Installation can not continue.";
		echo "  Add your output of /etc/lsb-release to the Installation script"
		exit
	fi
fi
sleep 1
###############################################
## INSTALL PACKAGES FROM SYSTEM REPOSITORIES ##
###############################################
echo ""
echo " ################################"
echo " # Installing dos2unix/tofrodos #"
echo " ################################"
echo ""
if $SEARCHCOMMAND dos2unix | grep -i -q 'dos2unix' ; then
	echo "  Dos2unix package found, installing latest version";
	$INSTALLCOMMAND dos2unix
else 	
	echo "  Dos2Unix package not found in repositories. Trying 'tofrodos'"
	if $SEARCHCOMMAND tofrodos | grep -i -q 'tofrodos'  ; then 
		echo "  Installing tofrodos and symlinking the fromdos to dos2unix command"
		$INSTALLCOMMAND tofrodos
		ln -s '/usr/bin/fromdos' '/usr/bin/dos2unix' 
	else 
		echo "  Dos2Unix/tofrodos is mandatory for conversion of uploaded data files.  "
		echo "  Please install manually and restart this script !"
		exit
	
	fi
fi
sleep 1
clear
echo ""
echo " #################################"
echo " # Installing mandatory packages #"
echo " #################################"
echo ""
if [ "$DISTRO" == "ubuntu" ] || [ "$DISTRO" == "debian" ] ; then
	echo "  About to install/update some necessary packages: rar, build-essential, r-base, r-cran-getopt, libxml2-dev, libcurl4-gnutls-dev, bc, wget, libperl-dev, libxp-dev, sendmail, texlive-latex-base, pdfjam, imagemagick, python-mysqldb, sshpass (+ all dependencies)."
	echo ""
	read -p "Install these packages [Y]/N: " -t 30 INSTALL
	if [ "$INSTALL" == "" ] || [ "$INSTALL" != "N" ] ; then  
		apt-get install -y build-essential rar r-base libxml2-dev libcurl4-gnutls-dev bc wget libperl-dev libxp-dev sendmail texlive-latex-base pdfjam imagemagick r-cran-getopt python-mysqldb sshpass
		echo "  => Install finished."
	fi
elif [ "$DISTRO" == "fedora" ]; then
	echo " About to install/update some necessary packages: rar, gcc, gcc-c++, kernel-devel, R-base, R-devel, R-getopt, libxml2-devel, libcurl-devel, bc, wget, libXp-devel, sendmail, perl-CPAN, ftp, perl-LWP-UserAgent-Determined, perl-ExtUtils-Embed, texlive-latex, pdfjam, imagemagick, MySQL-python, sshpass (+ all dependencies)."
	echo ""
	read -p "Install these packages [Y]/N: " -t 30 INSTALL
	if [ "$INSTALL" == "" ] || [ "$INSTALL" != "N" ] ; then  
		#yum groupinstall -y "Development Tools" "Development Libraries" "Perl Development" 
		yum install -y rar gcc gcc-c++ kernel-devel R-base R-devel libxml2-devel libcurl-devel libXp-devel sendmail perl-CPAN ftp perl-ExtUtils-Embed texlive-latex perl-LWP-UserAgent-Determined pdfjam ImageMagick MySQL-python sshpass
		echo "  => Install finished."
	fi
elif [ "$DISTRO" == "centos" ]; then
	echo " About to install/update some necessary packages: rar, php-gd, libxml2-devel, bc, wget, libXp-devel, sendmail, ftp, tetex-latex, imagemagick, MySQL-python, sshpass (+ all dependencies)."
	echo ""
	#echo "NOTE : phpmyadmin and R-core/R-devel are NOT found in the default centos repositories, and is taken from teh Fedora el5 repo instead !";
	#echo ""
	read -p "Install these packages [Y]/N: " -t 30 INSTALL
	if [ "$INSTALL" == "" ] || [ "$INSTALL" != "N" ] ; then  
		yum install -y rar php-gd gcc gcc-c++ kernel-devel R-core R-devel libxml2-devel libXp-devel sendmail ftp tetex-latex ImageMagick MySQL-python sshpass
		rpm -i Files/pdfjam-1.21-1.fc9.noarch.rpm
		echo "  => Install finished."
	fi

elif [ "$DISTRO" == "gentoo" ]; then
	echo "  About to install/update some necessary packages: rar, libxml2, WWW-curl, libperl, bc, libXp,, texlive-latex, pdfjam, imagemagick, libwww-perl, ftp, sudo, mysql-python, sshpass (+ all dependencies)."
	echo ""
	read -p "Install these packages [Y]/N: " -t 30 INSTALL
	if [ "$INSTALL" == "" ] || [ "$INSTALL" != "N" ] ; then  
		emerge --newuse -u rar gcc libxml2 WWW-Curl libperl libXp texlive-latex pdfjam imagemagick libwww-perl ftp bc sudo mysql-python sshpass
		# extUtils ook ?
		echo "  => Install finished."
	fi
else 
	echo "Distro not yet supported. install packages manually !"
	exit;
	
fi

sleep 5
clear

##########################
## INSTALL PERL MODULES ##
##########################
echo " ###########################"
echo " # INSTALLING CPAN MODULES # "
echo " ###########################"
# start CPAN
echo ""
echo "Installing perl LWP, Sys::CPU, Sys::CpuLoad, Number::Format, threads::shared, Thread::Queue, Getopt::Std modules"
echo ""
echo "NOTE : These modules are needed for the communication with the analysis programs."
echo "NOTE : Modules will only be installed if not present. They will NOT be auto-updated."
echo "       If you experience issues with them aftwards, update them manually.";
echo ""
read -p "  Hit enter to continue : " DUMMY
perl getCpanModules.pl 'LWP'
perl getCpanModules.pl 'Sys::CPU'
perl getCpanModules.pl 'Sys::CpuLoad'
perl getCpanModules.pl 'Number::Format'
perl getCpanModules.pl 'threads::shared'
perl getCpanModules.pl 'Thread::Queue'
perl getCpanModules.pl 'Getopt::Std'
perl getCpanModules.pl 'XML::Simple'
echo "  => All CPAN MODULES INSTALLED "

sleep 3 
clear

echo " #######################"
echo " # INSTALLING BiocLite #"
echo " #######################"
echo ""
echo "  Do we need to install the full biocLite tree with the 'biocLite()' command. This takes a long time and you might already have it. "
echo "  Also, if you are installing on a pure webserver, you will not need the BiocLite and other R packages. "
echo ""
read -p "  Install biocLite() [Y]/N : "  BIOC
if [ "$BIOC" != 'N' ] ; then
	cd $INSTALLDIR
	echo "  Installing BiocLite"
	Rscript RbiocLite.R
fi

read -p "  Install/Update Other BiocLite Packages (See Rpreparation.R file) [Y]/N : " BIOC
if [ "$BIOC" != 'N' ] ; then
	cd $INSTALLDIR
	echo "  Installing Needed R-Packages"
	Rscript Rpreparation.R
fi

echo "  => R packages installation finished"
sleep 2
clear


echo ""
echo " ####################################"
echo " # REPOSITORY INSTALLATION COMPLETE #"
echo " ####################################"
echo ""
echo "  Installation of public repository packages is complete (including CPAN). "
echo "  We will now continue with installation of CNV-WebStore specific files." 
echo "  You will first be prompted to provide some installation locations and user settings."
echo "  It is safe (and recommended) to leave all at default values."
echo ""
read -p "Press Enter to continue"  DUMMY

## DETECT THE APACHE RUNTIME USER
clear
echo ""
echo " #######################"
echo " # PROBING APACHE USER #"
echo " #######################"

if id wwwrun 2>/dev/null | grep -q 'uid' ; then 
	WWWUSER='wwwrun'
elif id www-data 2>/dev/null | grep -q 'uid' ; then 
	WWWUSER='www-data'
elif id apache 2>/dev/null | grep -q 'uid' ; then
	WWWUSER='apache'
else
	echo "  None of the typical apache users was present (wwwrun/www-data). Please provide the apache user, based on which some scheduled server-cleanups are performed."
	read -p "Apache User: [apache] " WWWUSER
	if [ "$WWWUSER" == '' ]; then 
		WWWUSER="apache"
	fi
fi
echo "  Apache User: $WWWUSER"
sleep 3
clear

## get the IP ADDRESS ( global host IP, change the hostname if wanted)
IP=`ifconfig eth0 | grep inet | grep -v inet6 | cut -d ":" -f 2 | cut -d " " -f 1`
if [ "$IP" == "" ]; then 
	# perhaps second ethernet card?
	IP=`ifconfig eth1 | grep inet | grep -v inet6 | cut -d ":" -f 2 | cut -d " " -f 1`
fi

echo ""
echo " ################################"
echo " # Provide Server Base Location #"
echo " ################################"
echo "  Provide the Base domain of the server. If you don't know this, use the provided IP-address."
echo "  A typical example is 'www.google.be' (without the quotes)"
read -p "    Server Domain: [$IP] " HOSTBASE
if [ "$HOSTBASE" == "" ]; then
	HOSTBASE=$IP
fi

clear

####################################
## WHERE TO PUT CNV-WEBSTORE TREE ##
####################################
echo ""
echo " ##################################################"
echo " # Provide Location For CNV-WebStore Program Tree #"
echo " ##################################################"
echo "  Enter the location to install the CNV-WebStore Program Tree : "
echo "  This directory will contain all CNV-WebStore related data. "
echo ""
echo "    THIS LOCATION MUST BE IDENTICAL ON ALL NODES AND THE WEB-INTERFACE !!."
echo ""
echo "The Web-Interface will be linked from here to the web-root."
echo "  This directory will be owned by a dedicated used for running CNV-analysis jobs. So if you already have this user, you can use a subdirectory of it's home location"
echo ""
read -p "  Installation Directory: [/CNV-WebStore/] " BASEDIR
if [[ "$BASEDIR" == "/" || "$BASEDIR" == "/boot/" || "$BASEDIR" == "/dev/" || "$BASEDIR" == "/home/" || "$BASEDIR" == "/lib/" || "$BASEDIR" == "/lib64/" || "$BASEDIR" == "/root/" || "$BASEDIR" == "/usr/" || "$BASEDIR" == "/bin/" || "$BASEDIR" == "/sbin/" ]]; then
	echo "Provided invalid path. "
	BASEDIR=""
fi
if [ "$BASEDIR" == '' ]; then 
	echo "     => Using Default Location: /CNV-WebStore/"
	BASEDIR="/CNV-WebStore/"
fi

while [[ "${BASEDIR:(-1)}" == "/" ]]; do BASEDIR="${BASEDIR%/}"; done
BASEDIRNOSLASH=${BASEDIR}
BASEDIR="${BASEDIR}/"
 
if [ -d "$BASEDIR" ]; then
	echo ""
	BASEDIR=${BASEDIR%/}
	echo "  $BASEDIR/ exists. Should I back it up to ${BASEDIR}.$(date +%Y%m%d%H%M)?"
	echo "  If you select 'no', we will add new directories, or update them using 'hg -u pull'"
	echo ""
	read -p "Backup Existing $BASEDIR/ ? Y/[N]" YESNO
	if [ "$YESNO" == "Y" ] || [ "$YESNO" == "y" ] ; then
		echo "     => Moving $BASEDIR to ${BASEDIR}.$(date +%Y%m%d%H%M) and creating fresh one."
		mv "$BASEDIR" "${BASEDIR}.$(date +%Y%m%d%H%M)"
		mkdir "$BASEDIR"
	fi
	BASEDIR=${BASEDIR}/	
else
	mkdir "$BASEDIR"
fi

sleep 2
clear

## WHERE TO INSTALL WEB PLATFORM?
if [ -d "/srv/www/htdocs/" ]; then
	SITEDIR="/srv/www/htdocs/"
elif [ -d "/var/www/html/" ]; then
	SITEDIR="/var/www/html/"
elif [ -d "/var/www/localhost/htdocs/" ]; then
	SITEDIR="/var/www/localhost/htdocs/"
elif [ -d "/var/www/" ]; then
	SITEDIR="/var/www/"
elif [ -d "/usr/local/apache2/htdocs/" ]; then
	SITEDIR="/usr/local/apache2/htdocs/"
elif [ -d "/Library/WebServer/Documents/" ]; then
	SITEDIR="/Library/WebServer/Documents/"
elif [ -d "/usr/pkg/share/httpd/htdocs/" ]; then
	SITEDIR="/usr/pkg/share/httpd/htdocs/" 
elif [ -d "/usr/local/www/apache22/data/" ]; then
	SITEDIR="/usr/local/www/apache22/data/"
elif [ -d "/usr/local/www/data/" ]; then
	SITEDIR="/usr/local/www/data/"
elif [ -d "/var/apache2/htdocs/" ]; then
	SITEDIR="/var/apache2/htdocs/"
elif [ -d "/svr/httpd/htdocs/" ]; then
	SITEDIR="/svr/httpd/htdocs/"
fi

## WHERE TO INSTALL WEB PLATFORM?
REDO=0
SYSTEMSITE=${SITEDIR}
while :
do
  echo ""
  echo " ###################################################"
  echo " # Provide Location For CNV-WebStore Web Interface #"
  echo " ###################################################"
  echo "  Enter the location in the webroot to create the CNV-WebStore website : "
  echo "  This directory will be a link to the CNV-WebStore BaseDir"
  echo "  The default location is the first found item of a predefined list of typical apache root-dir options."
  echo "  This location can not exists yet. If you specify an existing link/directory/file, "
  echo "  you will be asked to move it to a backup location or to to specify another directory."
  echo ""

  ## print installation option
  read -p "  Installation Directory: [$SITEDIR""cnv-webstore/] "  SDIR
  if [[ "$SDIR" == "/" || "$SDIR" == "/boot/" || "$SDIR" == "/dev/" || "$SDIR" == "/home/" || "$SDIR" == "/lib/" || "$SDIR" == "/lib64/" || "$SDIR" == "/root/" || "$SDIR" == "/usr/" || "$SDIR" == "/bin/" || "$SDIR" == "/sbin/" ]]; then
	echo "Provided invalid path.  Using default : $SITEDIR""/cnv-webstore"
	SDIR=""
  fi

  ## redirect installation to web root
  if [ "x"$SDIR == "x"$SITEDIR ]; then
	echo "Installation of CNV-WebStore into the Apache document root is not supported (Mercurial will not clone into non-empty dir.)";
	echo "Therefore, we will use the default location ($SITEDIR""/cnv-webstore). You can use redirection in the .htaccess file like this:"
	echo ""
	echo "  Options +FollowSymLinks +SymLinksIfOwnerMatch"
	echo "  RewriteEngine on"
	#echo "  RewriteRule ^$ /cnv-webstore/ [R]
	echo "  RewriteRule ^(.*) /cnv-webstore$1 [L]"
	#echo "  RewriteRule ^CNV-WebStore2$ /CNV-WebStore2/ [R]
	#RewriteRule ^CNV-WebStore2(.*) http://cmgcluster0/cnv-webstore$1 [P]
	echo ""
	echo "" 
	read -p "Hit enter to continue." DUMMY 
	SDIR=""
  fi
 
  if [ "$SDIR" == '' ]; then 
	if [ "$SITEDIR" == "" ]; then
		echo "  A directory to install the web interface is mandatory and could not be found automatically. Install will exit!"
		exit
	fi
	#CGIBIN="$SITEDIR""cgi-bin/"
	SITEDIR="$SITEDIR""cnv-webstore/"
	#SPATH="cnv-webstore/"
	PART="cnv-webstore"
	HOSTFULL="http://""$HOSTBASE""/cnv-webstore/"
  else
	while [[ "${SDIR:(-1)}" == "/" ]]; do SDIR="${SDIR%/}"; done 
	PART=${SDIR/$SITEDIR/}
	#SPATH=$PART
	#CGIBIN="$SITEDIR""cgi-bin"
	HOSTFULL="http://"$HOSTBASE"/"$PART"/"
	SITEDIR=$SDIR
  fi

  while [[ "${SITEDIR:(-1)}" == "/" ]]; do SITEDIR="${SITEDIR%/}"; done
  SITEDIRNOSLASH=${SITEDIR}
  SITEDIR="${SITEDIR}/"

  if [ -d "$SITEDIR" ]; then
	echo ""
	SITEDIR=${SITEDIR%/}
	echo "  $SITEDIR/ exists. Should I back it up to ${SITEDIR}.$(date +%Y%m%d%H%M)?"
	echo "  If you select 'no', You will hve to provide another directory location to install to."
	echo ""
	read -p "Backup Existing $SITEDIR/ ? Y/[N]" YESNO
	if [ "$YESNO" == "Y" ] || [ "$YESNO" == "y" ] ; then
		echo "     => Moving $SITEDIR to ${SITEDIR}.$(date +%Y%m%d%H%M) and creating fresh one."
		mv "$SITEDIR" "${SITEDIR}.$(date +%Y%m%d%H%M)"
		#mkdir "$SITEDIR"
		SITEDIR=${SITEDIR}/
		break
	else 
		SITEDIR=${SYSTEMSITE}
	fi
  else
	break
  fi
done 
echo "    => using $SITEDIRNOSLASH"
#mkdir "$SITEDIR"
sleep 2
clear

# WHERE TO STORE runtime datafiles and runtime temp output files
echo ""
echo " ########################################################"
echo " # Provide Location For CNV-WebStore Whole Genome Plots #"
echo " ########################################################"
echo "  Various intermediate datafiles are generated during CNV analysis that are passed around between"
echo "  consecutive steps of the analysis. These files are deleted after the analysis, but need to be accessible by all"
echo "  computing nodes AND the webserver. "
echo ""
echo "  Please specify the directory to store these files here. By default, it will be placed inside the CNV-WebStore BaseDir."
echo "  If you need to move it later one, replace the path in the generated "$BASEDIR"/.Credentials/.credentials file"
echo ""
echo "" 
echo " NOTE: This directory needs to be shared between all nodes AND the Web-Interface by NFS, or users will not be able to download plots!"
echo ""

read -p "  Data Storage: [$BASEDIR"Datafiles"] " DATADIR
if [[ "$DATADIR" == '' || "$DATADIR" == "/" || "$DATADIR" == "/boot/" || "$DATADIR" == "/dev/" || "$DATADIR" == "/home/" || "$DATADIR" == "/lib/" || "$DATADIR" == "/lib64/" || "$DATADIR" == "/root/" || "$DATADIR" == "/usr/" || "$DATADIR" == "/bin/" || "$DATADIR" == "/sbin/" ]]; then
	echo "  Invalid or empty location specified: using default"
        DATADIR=$BASEDIR"Datafiles/"
fi
while [[ "${DATADIR:(-1)}" == "/" ]]; do DATADIR="${DATADIR%/}"; done
DATADIR="${DATADIR}/"

if [ -d "$DATADIR" ]; then
	echo ""
	DATADIR=${DATADIR%/}
	echo "  $DATADIR/ exists. Should I back it up to ${DATADIR}.$(date +%Y%m%d%H%M)?"
	echo "  If you select 'no', we will use this location as is. This is the best options when the NFS share already exists"
	echo ""
	read -p "Backup Existing $DATADIR/ ? Y/[N]" YESNO
	if [ "$YESNO" == "Y" ] || [ "$YESNO" == "y" ] ; then
		echo "     => Moving $DATADIR to ${DATADIR}.$(date +%Y%m%d%H%M) and creating fresh one."
		mv "$DATADIR" "${DATADIR}.$(date +%Y%m%d%H%M)"
		mkdir "$DATADIR"
	fi
	DATADIR=${DATADIR}/	
else
	mkdir "$DATADIR"
fi

sleep 2
clear

# WHERE TO STORE plot pdfs
echo ""
echo " ########################################################"
echo " # Provide Location For CNV-WebStore Whole Genome Plots #"
echo " ########################################################"
echo "  Whole Genome Plots generated for analysed samples will be stored as PDF files."
echo "  As the number of analysed samples increases, this storage might take up lots of disk space."
echo "  Please specify the directory to store these plots here. By default, it will be placed inside the CNV-WebStore BaseDir."
echo "  If you need to move it later one, replace the path in the generated "$BASEDIR"/.Credentials/.credentials file"
echo ""
echo "" 
echo " NOTE: This directory needs to be shared between all nodes AND the Web-Interface by NFS, or users will not be able to download plots!"
echo ""

read -p "  Plot Storage: [$BASEDIR""CNVplots] " PLOTS
if [[ "$PLOTS" == '' || "$PLOTS" == "/" || "$PLOTS" == "/boot/" || "$PLOTS" == "/dev/" || "$PLOTS" == "/home/" || "$PLOTS" == "/lib/" || "$PLOTS" == "/lib64/" || "$PLOTS" == "/root/" || "$PLOTS" == "/usr/" || "$PLOTS" == "/bin/" || "$PLOTS" == "/sbin/" ]]; then
	echo "  Invalid or empty location specified: using default"
        PLOTS=$BASEDIR"CNVplots/"
fi
while [[ "${PLOTS:(-1)}" == "/" ]]; do PLOTS="${PLOTS%/}"; done
PLOTS="${PLOTS}/"

if [ -d "$PLOTS" ]; then
	echo ""
	PLOTS=${PLOTS%/}
	echo "  $PLOTS/ exists. Should I back it up to ${PLOTS}.$(date +%Y%m%d%H%M)?"
	echo "  If you select 'no', we will add new directories if needed. This is the best options when the NFS share already exists"
	echo ""
	read -p "Backup Existing $PLOTS/ ? Y/[N]" YESNO
	if [ "$YESNO" == "Y" ] || [ "$YESNO" == "y" ] ; then
		echo "     => Moving $PLOTS to ${PLOTS}.$(date +%Y%m%d%H%M) and creating fresh one."
		mv "$PLOTS" "${PLOTS}.$(date +%Y%m%d%H%M)"
		mkdir "$PLOTS"
		mkdir $PLOTS"/WGP"
		mkdir $PLOTS"/BAFSEG"
	else
		if [ !-d "$PLOTS/WGP" ] ; then
			mkdir "$PLOTS/WGP"
		fi
		if [ !-d "$PLOTS/BAFSEG" ]; then
			mkdir "$PLOTS/BAFSEG" 
		fi 
	fi
	PLOTS=${PLOTS}/	
else
	mkdir "$PLOTS"
	mkdir $PLOTS"WGP"
	mkdir $PLOTS"BAFSEG"
fi

sleep 2
clear

# WHERE TO STORE JOB OUTPUT
echo ""
echo " #########################################"
echo " # Provide Location For Job output files #"
echo " #########################################"
echo "  The analysis pipeline produces various output files that can be accessed by the user."
echo "  This way they can check where certain artefects or strange results arose (QC values, algorithm logs, etc)."
echo "  Please specify the directory to store these plots here. By default, it will be placed inside the directory specified above."
echo "  If you need to move it later one, replace the path in the generated "$BASEDIR"/.Credentials/.credentials file"
echo "" 
echo " NOTE: This directory needs to be shared between all nodes AND the Web-Interface by NFS, or users will not be able to access the files!"
echo ""

read -p "  Job Output Storage: [$BASEDIR""Cluster_Job_Output] " CJO
if [[ "$CJO" == '' || "$CJO" == "/" || "$CJO" == "/boot/" || "$CJO" == "/dev/" || "$CJO" == "/home/" || "$CJO" == "/lib/" || "$CJO" == "/lib64/" || "$CJO" == "/root/" || "$CJO" == "/usr/" || "$CJO" == "/bin/" || "$CJO" == "/sbin/" ]]; then
	echo "  Invalid or empty location specified: using default"
        CJO=$BASEDIR"Cluster_Job_Output/"
fi
while [[ "${CJO:(-1)}" == "/" ]]; do CJO="${CJO%/}"; done
CJO="${CJO}/"

if [ -d "$CJO" ]; then
	echo ""
	CJO=${CJO%/}
	echo "  $CJO/ exists. Should I back it up to ${CJO}.$(date +%Y%m%d%H%M)?"
	echo "  If you select 'no', we will add new directories if needed. This is the best options when the NFS share already exists"
	echo ""
	read -p "Backup Existing $CJO/ ? Y/[N]" YESNO
	if [ "$YESNO" == "Y" ] || [ "$YESNO" == "y" ] ; then
		echo "     => Moving $CJO to ${CJO}.$(date +%Y%m%d%H%M) and creating fresh one."
		mv "$CJO" "${CJO}.$(date +%Y%m%d%H%M)"
		mkdir "$CJO"
		chmod -R 777 "$CJO"
	fi
	CJO=${CJO}/	
else
	mkdir "$CJO"
	chmod -R 777 "$CJO"
fi

sleep 2
clear


## DOWNLOAD FILES FROM BITBUCKET
echo ""
echo " ################################################"
echo " # DOWNLOADING CNV-WebStore Web-Interface Files #"
echo " ################################################"
echo ""
echo " 1/ Credentials scripts"
cd $BASEDIR
hg clone https://bitbucket.org/medgenua/.credentials .Credentials

echo " 2/ Web-Interface:"  
hg clone https://bitbucket.org/medgenua/web-interface Web-Interface
ln -s $BASEDIRNOSLASH/Web-Interface $SITEDIRNOSLASH

echo " 3/ Maintenance Scripts:"
hg clone https://bitbucket.org/medgenua/servermaintenance ServerMaintenance
ln -s  $BASEDIRNOSLASH/ServerMaintenance/LiftOver/Files $BASEDIRNOSLASH/Web-Interface/LiftOver/Files

echo " 4/ CNVanalysis Configuration and runtime files:"
hg clone https://bitbucket.org/medgenua/cnvanalysis CNVanalysis
echo "    => Done";

echo " 5/ Database revisioning system:"
hg clone https://bitbucket.org/medgenua/database_revisions Database_Revisions
echo "    => Done";

echo ""
sleep 3

clear
ADMINEMAIL=''
echo ""
echo " #######################################################"
echo " # Provide needed usernames, passwords and admin email #"
echo " #######################################################"
echo ""
## administrator email
echo "  Please provide the email of the system administrator. This email will be used to sent username requests and backup failure notices."
read -p "  Administrator Email: " ADMINEMAIL
IFS="@"
set -- $ADMINEMAIL
if [ "${#@}" -ne 2 ] ; then
    echo "  Invalid Email provided"
    ADMINEMAIL=""
else
   echo "  Email Accepted"
fi
while [ "$ADMINEMAIL" == "" ] ; do
	echo "   Administrator email is mandatory. One day you will regret it if the backup failures did not arrive..."
	read -p "  Provide Administrator Email: " ADMINEMAIL
	## recheck
	IFS="@"
	set -- $ADMINEMAIL
	if [ "${#@}" -ne 2 ] ; then
	    echo "  Invalid Email provided"
	    ADMINEMAIL=""
	else
	    echo "  Email Accepted"
	fi
done
sleep 2
clear

# ASK for the script user and password
echo ""
echo "  To protect the system, a seperate user will be created to run the analysis scripts. "
echo "  This user will have its own home directory but should not be used. You can choose a very strong, or random password."
echo "  This password will be stored in a single file for perl-scripts and php-scripts (located in "$BASEDIR"/.Credentials/.credentials)"
echo ""
echo "  If the user does *not* exist yet, it will be created with the useradd command."
echo "  If you you will be using an ldap-based system, you should create an ldap user first and provide it here."
echo ""
read -p "  Username for dedicated Script-User: [scriptuser] " SCRIPTUSER
read -p "  Password for dedicated Script-User: [blank for random password] " -s SCRIPTPASS
echo ""

if [ "$SCRIPTUSER" == '' ]; then
	SCRIPTUSER='scriptuser'
fi
if [ "$SCRIPTPASS" == '' ]; then
	str0="$$"
	POS=2  # Starting from position 2 in the string.
	LEN=12  # Extract eight characters.
	str1=$( echo "$str0" | md5sum | md5sum )
	# Doubly scramble:     ^^^^^^   ^^^^^^
	SCRIPTPASS="${str1:$POS:$LEN}"
	# Can parameterize ^^^^ ^^^^
fi
echo "  Checking/Creating user '"$SCRIPTUSER"'"
## todo : add support for ldap auth user creation
USERCHECK=`id $SCRIPTUSER 2>&1`
if [[ $USERCHECK == "id: $SCRIPTUSER: No such user" ]]; then
	echo ""
	echo "NOTE : The user does not seem to exist."
	echo "NOTE : HOWEVER, this check is *NOT* reliable when using LDAP";
	echo ""
	read -p "Confirm that this user should be created as a local user: [Y]/N: " CONFIRM
	if [ "$CONFIRM" != "" -a "$CONFIRM" != "N" -a "$CONFIRM" != "n" ] ; then
		pass=$(perl -e 'print crypt($ARGV[0], "password")' $SCRIPTPASS)
		useradd -m -p $pass $SCRIPTUSER
		[ $? -eq 0 ] && echo "    => User has been added to system!" || echo "Failed to add the user!"
	else 
		echo "User was *NOT* added to the system. You should check if it exists in LDAP, or create it later on with the credentials in "$BASEDIR"/.Credentials/.credentials"
	fi
else
	echo "   $SCRIPTUSER exists!"
	echo "   Hopefully you entered the correct password. Otherwise, change it in "$BASEDIR"/.Credentials/.credentials file"
	echo ""
	read -p "   Hit enter to continue" DUMMY
fi
sleep 2
## determine user group
echo ""
echo "   Which usergroup should be used for file permission settings? The CNV-WebStore directories will be owned by the user specified before. Additional access can be set by specifying the correct usergroup. Please pick one from the list below: "
GROUPLIST=`id "$SCRIPTUSER" 2>&1 | perl -i -pe 's/.*groups=(.+)$/$1/g' | perl -i -pe 's/,/\n/g'`
COUNTER=1
ARRAY[0]='empty'
#echo "User Exists in the following user groups. Please Select The group to use for setting file permissions:"
echo ""
for i in ${GROUPLIST}
do
	[[ $i =~ [0-9]+\(([a-zA-Z0-9_]+)\) ]] 
	echo "    $COUNTER : ${BASH_REMATCH[1]}"
	ARRAY[$COUNTER]="${BASH_REMATCH[1]}"
	COUNTER=$[COUNTER+1]
done
read -p "  Usergroup to use: [1] : " USERGROUP

if [[ "$USERGROUP" -eq ""  || "$USERGROUP" -eq "0" || ! ${ARRAY[$USERGROUP]} ]] ; then
	echo "  Invalid choice. Using first entry."
	USERGROUP=${ARRAY[1]}
else
	USERGROUP=${ARRAY[$USERGROUP]}
fi
# backup to users if none available
if [[ "$USERGROUP" -eq "" ]] ; then
	USERGROUP="users"
fi
echo "    => USING : $USERGROUP"

######################################################################################
## DIRTY HACK FOR MATLAB ERROR...  && pdflatex error on centOS (and redhat/fedora?) ##
######################################################################################
if [[ $DISTRO == "centos" || $DISTRO == "redhat" || $DISTRO == "fedora" ]]; then
  if [ -d "/home/$SCRIPTUSER" ]; then
	## home dir exists, nothing to do
	echo "user home direcotry exists. nothing to do."
  else 
	mkdir "/home/$SCRIPTUSER"
	chown -R $SCRIPTUSER:$USERGROUP "/home/$SCRIPTUSER"
  fi
  mkdir "/home/$SCRIPTUSER/.texmf-var/"
  mkdir "/home/$SCRIPTUSER/.texmf-var/web2c/"
  ln -s "/home/$SCRIPTUSER/.texmf-var/" "/var/www/.texmf-var" 
  chown -R $SCRIPTUSER:$USERGROUP "/home/$SCRIPTUSER"
  
fi

# change ownership of BASEDIR
chown -R $SCRIPTUSER:$USERGROUP $BASEDIR


# ASK for the web platform mysql user and password
echo ""
echo "  Finally, provide the username, password and servername for access to the CNV-WebStore databases."
echo "  You should use the same user that was provided during creation of the CNV-WebStore Databases, "
echo "  for which all permissions are set."
echo "  The password will be stored in a single file for perl-scripts and php-scripts (located in "$BASEDIR"/.Credentials/.credentials)"
echo ""
echo "" 
echo " Furthermore, this user must be given access to a mysql server on the local host, using database CNVanalysis-TMP"
echo " To do so, you will be asked for the mysql root password. If the database exists, nothing will be changed."
echo ""

read -p "  Username for dedicated mysql-user: [webstore] " DBUSER
if [ "$DBUSER" == '' ]; then
	DBUSER='webstore'
fi
read -p "  Password for dedicated mysql-user: [blank for no password] " -s DBPASS
echo ""
read -p "  Hostname of the *MAIN* mysql database underlying the web-interface (make sure you enabled remote privileges for root if needed): [localhost] " DBHOST
echo ""
if [ "$DBHOST" == "" ]; then
	DBHOST='localhost'
fi

echo "Please provide the root password now for the mysql database on *THIS* machine, to grant '$DBUSER' access to the CNVanalysis-TMP database"
echo "Acces will be granted from the localhost, 127.0.01, current IP and current FQDN by default."
echo "This database will be cleared after every analysis."
echo ""

FQDN=`hostname -f`
HOSTNAME=`hostname`

mysql -u root -p -e "GRANT ALL PRIVILEGES ON \`CNVanalysis-TMP\`.* TO '$DBUSER'@'127.0.0.1' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON \`CNVanalysis-TMP\`.* TO '$DBUSER'@'localhost' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON \`CNVanalysis-TMP\`.* TO '$DBUSER'@'$FQDN' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON \`CNVanalysis-TMP\`.* TO '$DBUSER'@'$HOSTNAME' IDENTIFIED BY '$DBPASS'";


## check connection 
echo ""
echo "Checking Connection to database with given credentials."
mysql -u $DBUSER -p$DBPASS -e "SHOW databases;" > /dev/null
RETVAL=$?
if [ "$RETVAL" -eq "0" ]; then 
	echo "  => Connection established. "
else
	echo ""
	echo "  ######################"
	echo "  ## CRITICAL PROBLEM ##"
	echo "  ######################"
	echo ""
	echo "    => Connection Failed. Installation will be continue but no databases has been created  on the localhost ! "
	echo "    => You will have to create these databases yourself, by importing the 'CNVanalysis-TMP.sql' into mysql."
	echo "    => This file is located in the 'Database.tar.gz' archive in the 'Database' subdir of the current directory."
	echo ""
	read -p "Hit Enter to continue" DUMMY
	
fi

sleep 5 

echo "Creating the temporary databases if main is not on localhost"
cd $INSTALLDIR
cd Database/
tar xzvf Database.tar.gz CNVanalysis-TMP.sql
mysql -u $DBUSER -p$DBPASS < CNVanalysis-TMP.sql

echo " => Done."
sleep 3
clear

# ASK for the decipher Data Display Agreement Password
echo ""
echo "  DECIPHER, a public database of annotated chromosomal imbalances, allows the display of their data under"
echo "  specific conditions.  To display the data in CNV-WebStore, you need to provide the user and passwords  "
echo "  provided after signing the Data Display Agreement (DDA). If you do not have these passwords, this data will"
echo "  not be available in CNV-WebStore. Just leave the options below blank in this case."
echo "  This password will be stored in a single file for perl-scripts and php-scripts (located in /opt/ServerMaintenance/.credentials)"
echo ""
read -p "  Username for Decipher ftp-server: [] " DECIPHERUSER
if [ "$DECIPHERUSER" == "" ]; then
	DECIPHERUSER="NULL"
fi

read -p "  Password for Decipher ftp-server: [] " -s DECIPHERPASS
if [ "$DECIPHERPASS" == "" ]; then
        DECIPHERPASS="NULL"
fi
echo ""
read -p "  Password to decrypt Decipher data (gpg-passphrase): [] " -s DECIPHERGPGPASS
if [ "$DECIPHERGPGPASS" == "" ]; then
        DECIPHERGPGPASS="NULL"
fi
echo ""
sleep 2
clear


#################################
## write out .credentials file ##
#################################
if [ -f $BASEDIRNOSLASH/.Credentials/.credentials ]; then
	echo " ######################"
	echo " ## PROBLEM DETECTED ##"
	echo " ######################"
	echo ""
	echo "   The file '"$BASEDIRNOSLASH"/.Credentials/.credentials exists on this system and will *NOT* be updated automatically"
	echo ""
	echo "   These are the values that should be added to the  credentials file based on the current installation."
	echo "   Write them down and review them carefully before replacing existing values !"
	echo "		DBUSER=$DBUSER"
	echo "		DBPASS=$DBPASS"
	echo "		DBHOST=$DBHOST"
	echo "		SCRIPTUSER=$SCRIPTUSER"
	echo "		SCRIPTPASS=$SCRIPTPASS" 
	echo "		WWWRUN=$WWWUSER" 
	echo "		SITEDIR=$SITEDIR"
	echo "		WEBPATH=$PART" 
	echo "		ADMINMAIL=$ADMINEMAIL" 
	echo "		PLOTDIR=$PLOTS" 
	echo "		SCRIPTDIR=$BASEDIRNOSLASH/CNVanalysis"
	echo "          DATADIR=$DATADIR" 
	echo "		DECIPHERUSER=$DECIPHERUSER" 
	echo "		DECIPHERPASS=$DECIPHERPASS" 
	echo "		DECIPHERGPGPASS=$DECIPHERGPGPASS"
	echo "		USE_DRMAA=1" 
	echo "		CJO=$CJO" 
	echo "		MAINTENANCEDIR=$BASEDIRNOSLASH/ServerMaintenance"
	echo "          FTP=0"

	echo ""
	echo ""
	read -p "Hit any key to continue" DUMMY
else
	echo "DBUSER=$DBUSER" > $BASEDIRNOSLASH/.Credentials/.credentials
	echo "DBPASS=$DBPASS" >>  $BASEDIRNOSLASH/.Credentials/.credentials
	echo "DBHOST=$DBHOST" >>  $BASEDIRNOSLASH/.Credentials/.credentials
	echo "SCRIPTUSER=$SCRIPTUSER" >>  $BASEDIRNOSLASH/.Credentials/.credentials
	echo "SCRIPTPASS=$SCRIPTPASS" >>  $BASEDIRNOSLASH/.Credentials/.credentials
	echo "WWWRUN=$WWWUSER" >>  $BASEDIRNOSLASH/.Credentials/.credentials
	echo "SITEDIR=$SITEDIR" >>  $BASEDIRNOSLASH/.Credentials/.credentials
	echo "WEBPATH=$PART" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "ADMINMAIL=$ADMINEMAIL" >>  $BASEDIRNOSLASH/.Credentials/.credentials
	echo "PLOTDIR=$PLOTS" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "SCRIPTDIR=$BASEDIRNOSLASH/CNVanalysis" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "DATADIR=$DATADIR" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "DECIPHERUSER=$DECIPHERUSER" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "DECIPHERPASS=$DECIPHERPASS" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "DECIPHERGPGPASS=$DECIPHERGPGPASS" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "USE_DRMAA=1" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "CJO=$CJO" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "MAINTENANCEDIR=$BASEDIRNOSLASH/ServerMaintenance" >> $BASEDIRNOSLASH/.Credentials/.credentials
	echo "FTP=0" >> $BASEDIRNOSLASH/.Credentials/.credentials

fi
# change ownership to script user
chown $SCRIPTUSER:$USERGROUP  $BASEDIRNOSLASH/.Credentials/.credentials
chmod 755  $BASEDIRNOSLASH/.Credentials/.credentials

## create CLUSTER JOB OUTPUT dir
chmod 777 "$CJO"

# change ownership of analysis dir
if [ ! -d "$BASEDIR/CNVanalysis/output/" ]; then
	mkdir "$BASEDIR/CNVanalysis/output/"
fi
chown -R $SCRIPTUSER:$USERGROUP "$BASEDIR/CNVanalysis"
chmod -R 777 "$BASEDIR/CNVanalysis"

#change ownership of datadir
chown -R $SCRIPTUSER:$USERGROUP "$DATADIR"
chmod -R 777 "$DATADIR"
 
# make scriptuser the owner of webdir (for updating)
#chown -R $WWWUSER:$WWWUSER $SITEDIR
chown -R $SCRIPTUSER:$USERGROUP $SITEDIR

# make plotsdir writable to all
chmod -R 777 "$PLOTS"

clear

############################
## Finalise Web-Interface ##
############################
echo " ######################"
echo " # Finalising WebSite #"
echo " ######################"
mysql -h $DBHOST -u $DBUSER -p$DBPASS GenomicBuilds -e "UPDATE SiteStatus SET status = 'Install' WHERE 1" 
cd $SITEDIR
#echo "Replacing specific placeholders in files located at "$SITEDIR
# replace some stuff
#HOSTFREG=${HOSTFULL//\//\\\/}
#HOSTBREG=${HOSTBASE//\//\\\/}
#SPATHREG=${SPATH//\//\\\/}
#find -type f | xargs sed -i s/'SITEPATH'/"$SPATHREG"/g;
#find -type f | xargs sed -i s/'TARGETDIR'/"$LOCREG"/g;
#find -type f | xargs sed -i s/'TARGETREGDIR'/"$LOCREGREG"/g;
#find -type f | xargs sed -i s/'HOSTFULL'/"$HOSTFREG"/g;
#find -type f | xargs sed -i s/'HOSTBASE'/"$HOSTBREG"/g;
#find -type f -name '*.js' | xargs sed -i s/'SITEDIR'/"$SITEREG"/g;
#find -type f -name 'analyse.php' | xargs sed -i s/"'THESE ARE THE NEW VERSION COMMANDS'"/'$username'/g; 

# replace link to plots
rm Whole_Genome_Plots
ln -s $PLOTS Whole_Genome_Plots

# allow all to write in some needed directories
#chmod -R 777 $SITEDIRNOSLASH/status

#if [ ! -d "$SITEDIRNOSLASH/tracks" ] ; then
#	mkdir $SITEDIRNOSLASH/tracks
#fi
chmod -R 777 $SITEDIRNOSLASH/tracks
#if [ ! -d "$SITEDIRNOSLASH/bookmarks" ] ; then
#	mkdir $SITEDIRNOSLASH/bookmarks
#fi
chmod -R 777 $SITEDIRNOSLASH/bookmarks
#if [ ! -d "$SITEDIRNOSLASH/CNVlists" ] ; then
#	mkdir $SITEDIRNOSLASH/CNVlists
#fi
chmod -R 777 $SITEDIRNOSLASH/CNVlists
#if [ ! -d "$SITEDIRNOSLASH/data" ] ; then
#	mkdir $SITEDIRNOSLASH/data
#fi
chmod -R 777 $SITEDIRNOSLASH/data
#if [ ! -d "$SITEDIRNOSLASH/LiftOver" ] ; then
#	mkdir $SITEDIRNOSLASH/LiftOver
#fi
chmod -R 777 $SITEDIRNOSLASH/LiftOver

chmod -R 777 $SITEDIRNOSLASH/seqfiles
chmod -R 777 $SITEDIRNOSLASH/Reports
chmod -R 777 $SITEDIRNOSLASH/status

echo "  => Web Interface is in place."
sleep 2
clear 

echo ""
echo " ##############################"
echo " # ADDING APACHE USER TO SUDO #"
echo " ##############################"
## first recent systems with sudoers.d directory
if [ -d "/etc/sudoers.d" ]; then
	echo "  Creating $SCRIPTUSER file in /etc/sudoers.d/."
	echo "$WWWUSER	ALL=($SCRIPTUSER)	ALL" > /etc/sudoers.d/$SCRIPTUSER
	echo "Defaults:$WWWUSER       targetpw" >> /etc/sudoers.d/$SCRIPTUSER
	echo "Defaults:$WWWUSER	      !requiretty" >> /etc/sudoers.d/$SCRIPTUSER
	chmod 0440 /etc/sudoers.d/$SCRIPTUSER
else
	echo "     You are using an older version of sudo without /etc/sudoers.d/ support."
	echo "     The entry will be added to the end of the general /etc/sudoers file. This is a dangerous procedure. Check your sudoers file after installation, before reboot !";
	echo ""
	read -p "     Press Enter to continue" DUMMY
	chmod 600 /etc/sudoers
	if [ ! -z $WWWUSER ]; then cp /etc/sudoers /etc/sudoers.$(date +%Y%m%d%H%M); cat /etc/sudoers.$(date +%Y%m%d%H%M) | grep -v "$WWWUSER" > /etc/sudoers ; fi
	echo "$WWWUSER  ALL=($SCRIPTUSER)       ALL" >> /etc/sudoers
        echo "Defaults:$WWWUSER       targetpw" >> /etc/sudoers
        echo "Defaults:$WWWUSER       !requiretty" >> /etc/sudoers
	chmod 0440 /etc/sudoers
fi
clear

echo ""
echo " ######################"
echo " # ADAPT PHP.INI FILE #"
echo " ######################"
echo ""
echo "  The php.ini file should be updated to allow large file uploads and long running times. "
echo "  The values that have to be adapted are listed below (with updated values)"
echo "    - max_execution_time = 1200"
echo "    - max_input_time = 600"
echo "    - memory_limit = 1024M"
echo "    - post_max_size = 2047M"
echo "    - file_uploads = On"
echo "    - upload_max_filesize = 8000M"
echo "    - default_socket_timeout = 600"
echo "    - allow_url_fopen = On"
echo ""
PLOC=`locate -r 'apache2\/php.ini$' | wc -l`
if [ $PLOC == 1 ] ; then
	PLOC=`locate -r 'apache2\/php.ini$'`
	echo "  The php.ini file was located at '$PLOC'."
	read -t 30 -p "  Try automatic update? [Y]/N : " PHPUPDATE
else
	# gentoo location
	PLOC=`locate -r 'apache2-php5/php.ini' | wc -l`
 	if [ $PLOC == 1 ] ; then
		PLOC=`locate -r 'apache2-php5\/php.ini$'`
		echo "  The php.ini file was located at '$PLOC'."
		read -t 30 -p "  Try automatic update? [Y]/N : " PHPUPDATE
	else
		#check fi there is just one (cf fedora?) 
		PLOC=`locate -r 'php.ini$' | wc -l`
		if [ $PLOC == 1 ] ; then
			PLOC=`locate -r 'php.ini$'`
			echo "  The php.ini file was located at '$PLOC'."
			read -t 30 -p "  Try automatic update? [Y]/N : " PHPUPDATE
		else 
			echo "  Could not determine the php.ini file location."
			echo "  You will have to adapt the php.ini file yourself."
			echo ""
			PHPUPDATE='N'
			read -p " Write down the values to adapt en press enter to continue " DUMMY
		fi
	fi
fi
	
if [ "$PHPUPDATE" == 'N' ] || [ "$PHPUPDATE" == 'n' ]; then
	echo "  You will have to adapt the php.ini file yourself."
	echo ""
	read -p "  Write down the values to adapt en press enter to continue " DUMMY
else
	echo "  Trying to update the ini file."
 	sed -i s/'.*max_execution_time = [0-9]*.*'/'max_execution_time = 1200'/g $PLOC
	sed -i s/'.*max_input_time = [0-9]*.*'/'max_input_time = 600'/g $PLOC
	sed -i s/'.*memory_limit = [0-9]*.*'/'memory_limit = 1024M'/g $PLOC
	sed -i s/'.*post_max_size = [0-9]*.*'/'post_max_size = 2047M'/g $PLOC
	sed -i s/'.*file_uploads = .*'/'file_uploads = On'/g $PLOC
	sed -i s/'.*upload_max_filesize = .*'/'upload_max_filesize = 8000M'/g $PLOC
	sed -i s/'.*default_socket_timeout = .*'/'default_socket_timeout = 600'/g $PLOC
	sed -i s/'.*allow_url_fopen = .*'/'allow_url_fopen = On'/g $PLOC
	#OUT=`$APACHERESTART`
fi

sleep 5 
clear

echo ""
echo ""
echo " ################# "
echo " # UPDATING CRON # "
echo " ################# "
echo ""
echo "	 Writing out old root-cronjobs"

cd /tmp/
crontab -l | grep -v $BASEDIRNOSLASH"/ServerMaintenance/" > oldcrons
echo "   Old root-cronjobs"
cat oldcrons
# adding new
echo "15 1 15 1,3,5,7,9,11 * "$BASEDIRNOSLASH"/ServerMaintenance/Annotation_scripts/update_refseq.pl" >> oldcrons
echo "15 1 15 2,4,6,8,10,12 * "$BASEDIRNOSLASH"/ServerMaintenance/Annotation_scripts/update_DGV.pl" >> oldcrons
echo "15 1 16 1,3,5,7,9,11 * "$BASEDIRNOSLASH"/ServerMaintenance/Annotation_scripts/update_Encode.pl" >> oldcrons
echo "15 1 16 2,4,6,8,10,12 * "$BASEDIRNOSLASH"/ServerMaintenance/Annotation_scripts/update_segdups.pl" >> oldcrons
echo "15 1 17 1,3,5,7,9,11 * "$BASEDIRNOSLASH"/ServerMaintenance/Annotation_scripts/update_cytoBand.pl" >> oldcrons
#echo "1 0 * * * "$BASEDIRNOSLASH"/ServerMaintenance/clean_oldfiles.sh" >> oldcrons
#echo "0 20 * * 0,2,4 "$BASEDIRNOSLASH"/ServerMaintenance/automysqlbackup.sh" >> oldcrons
echo ""
echo "   New cronjobs: "
cat oldcrons
crontab oldcrons
echo "   "
echo "  => Done"
sleep 2
clear

##########################
## If on SELINUX DISTRO ##
##########################
if [ -e "/etc/selinux/config" ]; then
	echo ""
	echo "" 
	echo " ###################";
	echo " # DISABLE SELINUX #";
	echo " ###################";
	echo ""
	echo "   SELinux is a very tight protection belt around some linux distributions. Although it might be usefull, ";
	echo "   it can be annoying. For example, it even blocks the sending of mail from a php webservice, and makes some ";
	echo "   crucial parts of CNV-WebStore unusable. ";
	echo ""
	echo "   You can now choose to disable it completely, or to set it to a permissive mode, where all 'should be' violations"
	echo "   will be reported to a log file, while actually allowing them. By default the second options will be used.";
	echo ""
	echo "    [1] Disable SELinux"
	echo "    [2] Run SELinux in Permissive mode"
	echo ""
	read -p "       Specify SELinux action: [2] " -t 15 SEL
	echo ""
	if [ $SEL == '1' ]; then 
		echo "   => Disabling SELinux completely"
		sed -i s/'SELINUX=enforcing'/'SELINUX=disabled'/ /etc/selinux/config  
	else 
		echo "   => Setting SELinux to permissive mode"
		sed -i s/'SELINUX=enforcing'/'SELINUX=permissive'/ /etc/selinux/config
	fi
fi
sleep 2
clear 

echo ""
echo ""
echo " ######################### "
echo " # INSTALLATION COMPLETE # "
echo " ######################### "
echo "" 
echo "    First, restart apache with '$APACHERESTART'." 
echo "    Then go to $HOSTFULL and complete setup of the web platform.";
echo "    You will be asked to create an administrator account for the platform."
echo "    This user will have access to the control data and will be able to perform maintenance from a seperate 'admin' menu."
echo ""
echo "    After that, go grab a beer and have a nice CNV analysis experience."
echo ""
echo "    You might also find it usefull to check some scripts located under "$BASEDIRNOSLASH"/ServerMaintenance."
echo "    They can be appended to the root crontab, to update annotations, and backup your data"
echo "    By defaults, backups are stored under /root/ServerBackup and you should add a script to relocate them to some NAS-device."
echo ""
echo ""
echo " #############################"
echo " ## STAND-ALONE VS. CLUSTER ##"
echo " #############################"
echo ""
echo "    The default installation type is an HPC based cluster configuration" 
echo "    HPC Cluster interaction is done using the perl-DRMAA bindings. This has been tested using Torque-PBS."
echo "    To deactivate this, set the USE_DRMAA to 0 in the "$BASEDIRNOSLASH"/.Credentials/.credentials file."
echo "    Installation tips and requirements for the HPC setup are provided in the README_HPC.txt file." 
echo ""
echo ""

read -p "  Hit enter to finish the installation" DUMMY






