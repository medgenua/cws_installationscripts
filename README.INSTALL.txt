


1. Preparation of the HPC 
=========================

CNV-WebStore interacts with a cluster through perl_drmaa bindings. An example setup guideline is provided in README.HPC.txt. 
It is highly recommended to use LDAP based authentication among the computing nodes to make sure the correct permissions are kept on all files. 


2. Installation of the databases
================================

Install the CNV-WebStore databases on a mysql server using the Install_databases.sh script. We recommend creating a dedicated mysql user that will have only access to the CNV-WEbStore databases. 


3. Installation of the Web-Interface
====================================

