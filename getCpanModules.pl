#!/bin/perl

## get command line arg
my $module = $ARGV[0];
if ($module eq '') {
	print "No Module name provided. Installer will exit.\n";
	exit();
}
eval
{
  ( my $toload = "$module.pm") =~ s/::/\//g;
  require $toload;
  $module->import();
};

unless($@)
{
	print "Module $module exists. Skipping installation\n";
	exit();
}
print "Installing $module from CPAN: \n";
system("perl -MCPAN -e 'install $module'");
