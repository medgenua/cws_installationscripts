#######################################################################
##								     ##
## INSTALLATION TIPS & REQUIREMENTS FOR CNV-WEBSTORE DRMAA INTERFACE ##
##								     ##
#######################################################################

########################
## 1. NEEDED SOFTWARE ##
########################
 1.1 Dependencies for all nodes/master:
 --------------------------------------
	install using distro package management: sendmail build-essential libperl-dev

 1.2 Cluster Master:
 -------------------
    Torque-server:
    --------------
	a. Tested with Torque 2.5.5 
	b. Download source 'http://www.clusterresources.com/downloads/torque/torque-2.5.5.tar.gz'
	c. unpack : tar xzvf torque-*
	d. CONFIGURE WITHOUT DRMAA SUPPORT : ./configure 
	e. make && make install
	f. echo "/usr/local/lib" >> /etc/ld.so.conf (or similar)
	g. ldconfig
	h. add startup script from contrib/init.d to /etc/init.d:
		- cp contrib/init.d/debian.pbs_mom /etc/init.d/pbs_mom && update-rc.d pbs_mom defaults
		- cp contrib/init.d/debian.pbs_server /etc/init.d/pbs_server && update-rc.d pbs_server defaults
	i. run './torque.setup <username>' 
	j. specify nodes configurations in $TORQUEHOME/server_priv/nodes file (usually /var/spool/torque)
	k. specify NFS mounts in $TORQUEHOME/mom_priv/config:
		- this prevents scp copy of results. 
		- If you have ssh keys setup for password free login, this is not needed. 
		- syntax (* for all hosts): $usecp *:/var/tmp/datafiles /var/tmp/datafiles
	l. restart server : /etc/init.d/pbs_server restart
	m. start pbs_mom : /etc/init.d/pbs/pbs_mom restart
	

    MAUI Scheduler:
    ---------------
	a. Tested with maui v3.3.1
	b. Download from http://www.adaptivecomputing.com/resources/downloads/maui/
	c. unpack : tar xzvf maui-*.tar.gz
	d. ./configure –-with-pbs
	e. make && make install
	f. add the following to your path : (/etc/bash.bashrc)
		PATH=$PATH:/usr/local/maui/bin:/usr/local/maui/sbin
	g. put init script in place, provided for suse/redhat.
	h. add to default runlevels (eg ubuntu): update-rc.d maui defaults 
	i. (re)start maui : /etc/init.d/maui restart

    Perl-DRMAA wrapper:
    -------------------
	a. Tested with Schedule-DRMAAc-0.81 => Do NOT install using CPAN, this version is for SGE !
	b. http://search.cpan.org/~tharsch/Schedule-DRMAAc-0.81/Schedule_DRMAAc.pod
	c. unpack : tar xzvf Schedule-DRMAAc-*  && cd Schedule-DRMAAc-*
	d. If NOT on a Sun Grid Engine Cluster : install swig from distro repositories
	e. Create link from /usr/local/include/drmaa.h to drmaa.h
	f. run : export SGE_ROOT='/usr/local'     => or adapt Makefile.PL (SGE-specific settings)
	g. run : swig -perl -Wall -exportall Schedule_DRMAAc.i
	h. perl Makefile.PL
	i. make && make install
	
 1.3 Cluster nodes:
 -----------------
	- Option 1 (recommended): share the /usr/local/ to Nodes using NFS
	- Option 2 : Repeat steps from 1.2 torque-server: 
		torque : b-c;
		torque : d : ./configure --enable-drmaa --disable-server;
		torque : e-g;
		torque : h : only pbs_mom;
		torque : m;
		maui : skip
		Perl-DRMAA: full 
	- copy the following files/directories from server to nodes (if not existing from option 2) (torquehome = /var/spool/ ?): 
		- $TORQUEHOME/aux
		- $TORQUEHOME/checkpoint/
		- $TORQUEHOME/mom_logs/
		- $TORQUEHOME/mom_priv   => make sure you have the 'config' file from server setup !
		- $TORQUEHOME/pbs_environment
		- $TORQUEHOME/server_name
		- $TORQUEHOME/spool/
		- $TORQUEHOME/undelivered/


##############################
## 2. INSTALL CNV-WebStore  ##
##############################
 2.1 : WebServer
 ---------------
	- This does not need to be the torque master
	- install dependencies (see sourceforge)
	- Install Full CNV-WebStore using the CNV-WebStore_INSTALL.sh script
	- follow all steps, make sure you setup wildcard hostnames in the mysql setup part. 
	- set USE_DRMAA to 1 in /opt/ServerMaintenance/.credentials to enable distributed computing.

 2.2. SHARED STORAGE SETUP 
 -------------------------
    The following locations need to be shared using NFS among all nodes from (best from WebServer) (hard coded): 
	- /home				: Torque working dir defaults to users home dir
	- /opt/ServerMaintenance	: Contains CNV-WebStore analysis flow
	- /var/tmp/datafiles		: contains input/output files uploaded downloaded by users. 
					    Gets cleaned up afterwards.
	
    The following locations need to be shared using NFS among all nodes (from .credentials)
	- CJO  				: Cluster_Job_Output path 
					    collects all runtime output. (default /Cluster_Job_Output)

 2.3. USER SETUP 
 ---------------
    Make sure the SCRIPTUSER specified in the .credentials file exists on all nodes with the same password. 
    Easiest solution is to use LDAP authentication. 


 2.4 CNV-WebStore Nodes
 ----------------------
	- Install dependencies (see sourceforge, but apache/php not necessary, only mysql)
	- Install the needed components by running the CNV-WebStore_NODE_INSTALL.sh script
	- The installer checks if needed NFS mounts are present and if the SCRIPTUSER exists. 
