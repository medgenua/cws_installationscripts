#!/bin/bash

clear 
echo ""
echo " ################"
echo " # Introduction #"
echo " ################"
echo ""
echo "  This installation script will install the CNV-WebStore databases. "
echo "  It will ask you to create or provide a mysql user to access the results from the web interface. "
echo " "
echo "  IMPORTANT: Once you have entered the mysql-root password (to create the mysql user), do not interrupt the script, "
echo "  as (random) passwords have then be created and may conflict if the script is re-run. If installation or download of"
echo "  files fails before this step, you can safely abort (Ctrl-C) and retry installation. "
echo ""
read -p "  Hit enter to start the installation" -t 30 DUMMY

clear
## Where am I now?
INSTALLDIR=`pwd`


# ASK for the web platform mysql user and password
echo ""
echo "  Provide a username, password  and servername for access to the mysql database that will be used by CNV-WebStore."
echo "  This user will only exist for the mysql database, not as a real system user. If you provide an existing mysql-user, "
echo "  permissions to other databases should remain intact, but in this case provide the correct password !"
echo "  This password will be stored in the '.dbpass.txt' file in the current directory. You will need the password during the"
echo "  setup of the other CNV-WebStore components !"
echo ""
read -t 30 -p "  Username: [webstore] " DBUSER
if [ "$DBUSER" == '' ]; then
	DBUSER='webstore'
fi
read -t 30 -p "  Password for mysql-user: [blank for new, random password]" -s DBPASS
echo ""
if [ "$DBPASS" == '' ]; then
	str0="$$"
	POS=2  # Starting from position 2 in the string.
	LEN=12  # Extract 12 characters.
	str1=$( echo "$str0" | md5sum | md5sum )
	# Doubly scramble:     ^^^^^^   ^^^^^^
	DBPASS="${str1:$POS:$LEN}"
	# Can parameterize ^^^^ ^^^^
fi
read -t 30 -p "  Hostname of the mysql database (make sure you enabled remote privileges for root if needed): [localhost] " DBHOST
echo ""
if [ "$DBHOST" == "" ]; then
	DBHOST='localhost'
fi
echo ""
echo " If you need wildcard access (for cluster environment for example), specify it here."
echo ""
read  -p "  Wildcard Hostname for mysql access (leave blank if unsure) : " WILDCARD

## get the hostname, IP, etc ( global host IP, change the hostname if wanted)
IP=`ifconfig eth0 | grep inet | grep -v inet6 | cut -d ":" -f 2 | cut -d " " -f 1`
if [ "$IP" == "" ]; then 
	# perhaps second ethernet card?
	IP=`ifconfig eth1 | grep inet | grep -v inet6 | cut -d ":" -f 2 | cut -d " " -f 1`
fi
if [ "$IP" == "" ]; then
	IP="127.0.0.1"
fi
FQDN=`hostname -f`
HOSTNAME=`hostname`

echo " You will now be pompted for the mysql password. "
echo " This is needed once, to login and create for the creation of the mysql user you just specified."
echo " This user will be granted acces from the localhost, 127.0.01, current IP and current FQDN by default."

if [ "$WILDCARD" == "" ]; then
	mysql -h $DBHOST -u root -p -e "GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'$DBHOST' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'127.0.0.1' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'localhost' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'$IP' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'$HOSTNAME' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'$FQDN' IDENTIFIED BY '$DBPASS'";
else 
	mysql -h $DBHOST -u root -p -e "GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'$DBHOST' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'127.0.0.1' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'localhost' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'$WILDCARD' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'$IP' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'$HOSTNAME' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'$FQDN' IDENTIFIED BY '$DBPASS'";
fi

## check connection 
echo ""
echo "Checking Connection to database with given credentials."
mysql -h $DBHOST -u $DBUSER -p$DBPASS -e "SHOW databases;" > /dev/null
RETVAL=$?
if [ "$RETVAL" -eq "0" ]; then 
	echo "  => Connection established. "
else
	echo "  => Connection Failed. Installation will be aborted. No databases has been created, but a mysql user '$DBUSER' might be created."
	exit
fi

sleep 5 


#################################
## write out .credentials file ##
#################################
echo "DBUSER=$DBUSER" > .dbpass.txt
echo "DBPASS=$DBPASS" >> .dbpass.txt
echo "DBHOST=$DBHOST" >> .dbpass.txt
clear
echo ""
echo " #######################"
echo " # Unpacking Databases #"
echo " #######################"
cd Database/
tar xzf Database.tar.gz
# load the database
echo "  Loading databases:"
echo "    => Structures";
mysql -h $DBHOST -u $DBUSER -p$DBPASS < GenomicBuilds.sql
rm -f GenomicBuilds.sql
mysql -h $DBHOST -u $DBUSER -p$DBPASS < LNDB.sql
rm -f LNDB.sql
mysql -h $DBHOST -u $DBUSER -p$DBPASS < MainDatabase.sql
rm -f MainDatabase.sql
mysql -h $DBHOST -u $DBUSER -p$DBPASS < CNVanalysis-TMP.sql
rm -f CNVanalysis-TMP.sql
NAME=`mysql --batch -u $DBUSER -p$DBPASS GenomicBuilds -e 'SELECT name FROM CurrentBuild'`
NAME=${NAME/name/}
NAME=${NAME//[[:space:]]/}
echo "    => Data";
mysql -h $DBHOST -u $DBUSER -p$DBPASS CNVanalysis$NAME < mainfilledtables.sql
rm -f mainfilledtables.sql
cd $INSTALLDIR
echo "  Loading Control Samples"
perl LoadControls.pl .dbpass.txt
# clear up some genomebuild tables
mysql -h $DBHOST -u $DBUSER -p$DBPASS GenomicBuilds -e 'TRUNCATE TABLE FailedLifts'
mysql -h $DBHOST -u $DBUSER -p$DBPASS GenomicBuilds -e 'TRUNCATE TABLE LiftsInProgress'
mysql -h $DBHOST -u $DBUSER -p$DBPASS GenomicBuilds -e 'TRUNCATE TABLE NewBuild'
mysql -h $DBHOST -u $DBUSER -p$DBPASS GenomicBuilds -e 'TRUNCATE TABLE PreviousBuilds'
mysql -h $DBHOST -u $DBUSER -p$DBPASS GenomicBuilds -e 'TRUNCATE TABLE hg18Tohg19UserPermissions'
mysql -h $DBHOST -u $DBUSER -p$DBPASS GenomicBuilds -e 'TRUNCATE TABLE hg18Tohg19cus_UserPermissions'
mysql -h $DBHOST -u $DBUSER -p$DBPASS CNVanalysis$NAME -e 'UPDATE sample SET clinical = NULL WHERE clinical = "NULL"'
echo "  => Database loaded."
sleep 2
clear

echo ""
echo " #############################"
echo " # APPLYING DATABASE UPDATES #"
echo " #############################"
echo ""
echo "    We will now look if updates to the mysql database are available, and if so, apply the updates.";
echo ""
if [ -d ../Database_Revisions/ ]; then
	cd ../Database_Revisions/ 
	perl ApplyUpdates.pl $INSTALLDIR/.dbpass.txt
else
	echo "Cloning the database_revisions to a temporary location."
	hg clone https://bitbucket.org/medgenua/database_revisions Database_Revisions

	cd Database_Revisions
	perl ApplyUpdates.pl $INSTALLDIR/.dbpass.txt
	cd ..
	rm -Rf Database_Revisions
fi

cd $INSTALLDIR
clear

echo ""
echo ""
echo " ######################### "
echo " # INSTALLATION COMPLETE # "
echo " ######################### "
echo "" 
read -p "  Hit enter to finish the installation of the databases" DUMMY






