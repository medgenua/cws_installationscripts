#!/bin/bash

#########################################
# GET SYSTEM DETAILS AND USER/PASSWORDS #
#########################################

if [[ $EUID -ne 0 ]]; then
  echo ""
  echo "#######################################################################################"
  echo "# The installation scripts must be run as root user (sudo ./Install_Analysis_only.sh) #" 2>&1
  echo "#######################################################################################"
  echo 
  exit 1
fi

clear 
echo ""
echo " ################"
echo " # Introduction #"
echo " ################"
echo ""
echo "  This installation script will install CNV-WebStore.  It will first install necessary packages from both the "
echo "  general repositories, CPAN an bioconductor. Then it will ask you to create a system user to run analysis scripts"
echo "  and a mysql user to access the results from the web interface. Most steps do not request interaction and will"
echo "  continue with default values after a few seconds. "
echo " "
echo ""
echo "  IMPORTANT: You will need to have a cluster environment running with DRMAA support . An example setup guide for Torque/PBS is included in the README.HPC file."
echo ""
echo "  IMPORTANT: Once you have entered the mysql-root password (to create the mysql user), do not interrupt the script, "
echo "  as (random) passwords have then be created and may conflict if the script is re-run. If installation or download of"
echo "  files fails before this step, you can safely abort (Ctrl-C) and retry installation. "
echo ""
read -p "  Hit enter to start the installation" DUMMY

clear
## Where am I now?
INSTALLDIR=`pwd`

## what system am I on?
echo ""
echo " ###########################"
echo " # PROBING FOR DISTRO TYPE #"
echo " ###########################"
if [ -e "/etc/debian_version" ]; then
	echo "  Detected Debian-Based Distro, installing software through APT"
	INSTALLCOMMAND='apt-get install -y'
	SEARCHCOMMAND='aptitude search'
	APACHERESTART='/etc/init.d/apache2 restart'
	DISTRO="debian"
elif [ -e "/etc/fedora-release" ]; then
	echo "  Detected Fedora-Based Distro, installing software through YUM"
	INSTALLCOMMAND='yum install -y'
	SEARCHCOMMAND='yum search'
	APACHERESTART='service httpd restart'
	DISTRO="fedora"
elif [ -e "/etc/gentoo-release" ]; then
	echo "  Detected Gentoo-Based Distro, installing software through EMERGE"
	INSTALLCOMMAND='emerge --newuse -u'
	SEARCHCOMMAND='emerge --search'
	DISTRO="gentoo"
elif [ -e "/etc/redhat-release" ]; then
	CONTENT=`cat "/etc/redhat-release"`
	if echo $CONTENT | grep -i "CentOS" ;then
		echo "  Detected CentOS Distro, installing softwere through YUM"
		INSTALLCOMMAND='yum install -y'
		SEARCHCOMMAND='yum search'
		APACHERESTART='service httpd restart'
		DISTRO="centos"
	else 
		echo "  Detected RedHat-Based Distro, installing softwere through YUM"
		INSTALLCOMMAND='yum install -y'
		SEARCHCOMMAND='yum search'
		APACHERESTART='service httpd restart'
		DISTRO="redhat"
	fi
elif [ -e "/etc/lsb-release" ]; then
	# generic
	CONTENT=`cat "/etc/lsb-release"`
	if echo $CONTENT | grep -i "ubuntu" ;then
		echo "  Detected Ubuntu Distro, installing softwere through APT"
		INSTALLCOMMAND='apt-get install -y'
		SEARCHCOMMAND='aptitude search'
		APACHERESTART='/etc/init.d/apache2 restart'
		DISTRO="ubuntu"
	else 
		echo "  Unknown distro type. Installation can not continue.";
		echo "  Add your output of /etc/lsb-release to the Installation script"
		exit
	fi
fi
sleep 1
###############################################
## INSTALL PACKAGES FROM SYSTEM REPOSITORIES ##
###############################################
echo ""
echo " ################################"
echo " # Installing dos2unix/tofrodos #"
echo " ################################"
echo ""
if $SEARCHCOMMAND dos2unix | grep -i -q 'dos2unix' ; then
	echo "  Dos2unix package found, installing latest version";
	$INSTALLCOMMAND dos2unix
else 	
	echo "  Dos2Unix package not found in repositories. Trying 'tofrodos'"
	if $SEARCHCOMMAND tofrodos | grep -i -q 'tofrodos'  ; then 
		echo "  Installing tofrodos and symlinking the fromdos to dos2unix command"
		$INSTALLCOMMAND tofrodos
		ln -s '/usr/bin/fromdos' '/usr/bin/dos2unix' 
	else 
		echo "  Dos2Unix/tofrodos is mandatory for conversion of uploaded data files.  "
		echo "  Please install manually and restart this script !"
	
	fi
fi
sleep 1
clear
echo ""
echo " #################################"
echo " # Installing mandatory packages #"
echo " #################################"
echo ""
if [ "$DISTRO" == "ubuntu" ] || [ "$DISTRO" == "debian" ] ; then
	echo "  About to install/update some necessary packages: rar, build-essential, r-base, r-cran-getopt, libxml2-dev, libcurl4-gnutls-dev, bc, wget, libperl-dev, libxp-dev, sendmail, texlive-latex-base, pdfjam, imagemagick, python-mysqldb, sshpass (+ all dependencies)."
	echo ""
	read -p "Install these packages [Y]/N: " -t 30 INSTALL
	if [ "$INSTALL" == "" ] || [ "$INSTALL" != "N" ] ; then  
		apt-get install -y build-essential rar r-base libxml2-dev libcurl4-gnutls-dev bc wget libperl-dev libxp-dev sendmail texlive-latex-base pdfjam imagemagick r-cran-getopt python-mysqldb sshpass
		echo "  => Install finished."
	fi
elif [ "$DISTRO" == "fedora" ]; then
	echo " About to install/update some necessary packages: rar, gcc, gcc-c++, kernel-devel, R-base, R-devel, R-getopt, libxml2-devel, libcurl-devel, bc, wget, libXp-devel, sendmail, perl-CPAN, ftp, perl-LWP-UserAgent-Determined, perl-ExtUtils-Embed, texlive-latex, pdfjam, imagemagick, MySQL-python, sshpass (+ all dependencies)."
	echo ""
	read -p "Install these packages [Y]/N: " -t 30 INSTALL
	if [ "$INSTALL" == "" ] || [ "$INSTALL" != "N" ] ; then  
		#yum groupinstall -y "Development Tools" "Development Libraries" "Perl Development" 
		yum install -y gcc gcc-c++ kernel-devel rar R-base R-devel libxml2-devel libcurl-devel libXp-devel sendmail perl-CPAN ftp perl-ExtUtils-Embed texlive-latex perl-LWP-UserAgent-Determined pdfjam ImageMagick R-getopt MySQL-python sshpass
		echo "  => Install finished."
	fi
elif [ "$DISTRO" == "centos" ]; then
	echo " About to install/update some necessary packages: rar, php-gd,gcc, gcc-c++, kernel-devel, libxml2-devel, bc, wget, libXp-devel, sendmail, ftp, tetex-latex, imagemagick, MySQL-python, sshpass (+ all dependencies)."
	echo ""
	echo "NOTE : phpmyadmin and R-core/R-devel are NOT found in the default centos repositories, and is taken from teh Fedora el5 repo instead !";
	echo ""
	read -p "Install these packages [Y]/N: " -t 30 INSTALL
	if [ "$INSTALL" == "" ] || [ "$INSTALL" != "N" ] ; then  
		yum install -y rar gcc gcc-c++ kernel-devel R-core R-devel libxml2-devel libXp-devel sendmail ftp tetex-latex ImageMagick MySQL-python sshpass
		rpm -i Files/pdfjam-1.21-1.fc9.noarch.rpm
		echo "  => Install finished."
	fi

elif [ "$DISTRO" == "gentoo" ]; then
	echo "  About to install/update some necessary packages:rar, phpmyadmin, gcc, R, libxml2, WWW-curl, libperl, bc, libXp,, texlive-latex, pdfjam, imagemagick, libwww-perl, ftp, sudo, mysql-python, sshpass (+ all dependencies)."
	echo ""
	read -p "Install these packages [Y]/N: " -t 30 INSTALL
	if [ "$INSTALL" == "" ] || [ "$INSTALL" != "N" ] ; then  
		emerge --newuse -u rar R gcc libxml2 WWW-Curl libperl libXp texlive-latex pdfjam imagemagick libwww-perl ftp bc sudo mysql-python sshpass
		# extUtils ook ?
		echo "  => Install finished."
	fi
else 
	echo "Distro not yet supported. install packages manually !"
	exit;
	
fi

sleep 5
clear

##########################
## INSTALL PERL MODULES ##
##########################
echo " ###########################"
echo " # INSTALLING CPAN MODULES # "
echo " ###########################"
# start CPAN
echo ""
echo "Installing perl LWP, Sys::CPU, Sys::CpuLoad, Number::Format, threads::shared, Thread::Queue, Getopt::Std modules"
echo ""
echo "NOTE : Modules will only be installed if not present. They will NOT be auto-updated."
echo "       If you experience issues with them aftwards, update them manually.";
echo ""
read -p "  Hit enter to continue : " DUMMY
perl getCpanModules.pl 'LWP'
perl getCpanModules.pl 'Sys::CPU'
perl getCpanModules.pl 'Sys::CpuLoad'
perl getCpanModules.pl 'Number::Format'
perl getCpanModules.pl 'threads::shared'
perl getCpanModules.pl 'Thread::Queue'
perl getCpanModules.pl 'Getopt::Std'
perl getCpanModules.pl 'XML::Simple'
echo "  => All CPAN MODULES INSTALLED "

sleep 3 
clear

echo " #######################"
echo " # INSTALLING BiocLite #"
echo " #######################"
echo ""
echo "  Do we need to install the full biocLite tree with the 'biocLite()' command. This takes a long time and you might already have it. "
echo "  Also, if you are installing on a pure webserver, you will not need the BiocLite and other R packages. "
echo ""
read -p "  Install biocLite() [Y]/N : "  BIOC
if [ "$BIOC" != 'N' ] ; then
	cd $INSTALLDIR
	echo "  Installing BiocLite"
	Rscript RbiocLite.R
fi

read -p "  Install/Update Other BiocLite Packages (See Rpreparation.R file) [Y]/N : " BIOC
if [ "$BIOC" != 'N' ] ; then
	cd $INSTALLDIR
	echo "  Installing Needed R-Packages"
	Rscript Rpreparation.R
fi

echo "  => R packages installation finished"
sleep 2
clear

echo ""
echo " ####################################"
echo " # REPOSITORY INSTALLATION COMPLETE #"
echo " ####################################"
echo ""
echo "  Installation of public repository packages is complete (including CPAN and Bioconductor). "
echo "  We will now continue with installation of CNV-WebStore temporary database." 
echo ""
read -p "Press Enter to continue" DUMMY

clear

# ASK for the web platform mysql user and password
echo "  ##########################################"
echo "  ## INSTALL THE MYSQL TEMPORARY DATABASE ##"
echo "  ##########################################"
echo ""
echo "  Provide the username, password and servername for access to the CNV-WebStore databases."
echo "  You should use the same user that was provided during creation of the Main CNV-WebStore Databases, "
echo "  for which all permissions are set."
echo "  This password is stored in a single file for perl-scripts and php-scripts (located in "$BASEDIR"/.Credentials/.credentials)"
echo ""
echo "" 
echo " This user will be given access to a mysql server on the local host, using database CNVanalysis-TMP"
echo " To do so, you will be asked for the mysql root password. If the database exists, nothing will be changed."
echo ""
echo " NOTICE: If you do not have a mysql database running yet, install one now following the distribution specific manuals."
echo ""

read -p "  Username for dedicated mysql-user: [webstore] " DBUSER
if [ "$DBUSER" == '' ]; then
	DBUSER='webstore'
fi
read -p "  Password for dedicated mysql-user: [blank for no password] " DBPASS
echo ""

echo "Please provide the root password now for the mysql database on *THIS* machine, to grant '$DBUSER' access to the CNVanalysis-TMP database"
echo "Acces will be granted from the localhost, 127.0.01, current IP and current FQDN by default."
echo "This database will be cleared after every analysis."
echo ""

FQDN=`hostname -f`
HOSTNAME=`hostname`

mysql -u root -p -e "GRANT ALL PRIVILEGES ON \`CNVanalysis-TMP\`.* TO '$DBUSER'@'127.0.0.1' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON \`CNVanalysis-TMP\`.* TO '$DBUSER'@'localhost' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON \`CNVanalysis-TMP\`.* TO '$DBUSER'@'$FQDN' IDENTIFIED BY '$DBPASS';GRANT ALL PRIVILEGES ON \`CNVanalysis-TMP\`.* TO '$DBUSER'@'$HOSTNAME' IDENTIFIED BY '$DBPASS'";


## check connection 
echo ""
echo "Checking Connection to database with given credentials."
mysql -u $DBUSER -p$DBPASS -e "SHOW databases;" > /dev/null
RETVAL=$?
if [ "$RETVAL" -eq "0" ]; then 
	echo "  => Connection established. "
else
	echo ""
	echo "  ######################"
	echo "  ## CRITICAL PROBLEM ##"
	echo "  ######################"
	echo ""
	echo "    => Connection Failed. Installation will be continue but no databases has been created  on the localhost ! "
	echo "    => You will have to create these databases yourself, by importing the 'CNVanalysis-TMP.sql' into mysql."
	echo "    => This file is located in the 'Database.tar.gz' archive in the 'Database' subdir of the current directory."
	echo ""
	read -p "Hit Enter to continue" DUMMY
	
fi

sleep 5 

echo "Creating the temporary databases"
cd $INSTALLDIR
cd Database/
tar xzvf Database.tar.gz CNVanalysis-TMP.sql
mysql -u $DBUSER -p$DBPASS < CNVanalysis-TMP.sql

echo " => Done."
sleep 3
clear



###########################################
## PICK INSTALLATION TYPE : LOCAL vs NFS ##
###########################################
echo ""
echo " #####################################"
echo " # FINALISE INSTALLATION : NFS SETUP #"
echo " #####################################"
echo ""
echo " CNV-WebStore uses NFS-based access to analysis files. "
echo " " 
echo "    1. NFS based: Share the complete CNV-WebStore tree on a NFS-server and mount on the "
echo "         nodes in the EXACT same location: "
echo "          - Benefit:  easy, single repository to maintain."
echo "          - Drawback: can be slower as a lot of data can go back and forth over the network"
echo ""
echo "    2. Local Install: Install the CNVanalsysis branch of CNV-WebStore on every node"
echo "          - Benefit:  Speeds up analysis by reducing network traffic"
echo "          - Drawback: You have to maintain multiple instances"
echo ""
echo "  We used option 1, where the CNVanalysis branch is accessible by the web-server. "
echo "  This allows updating the entire project from the web-interface."
echo ""
read -p "Hit enter to read the setup guidelines." DUMMY
 

clear
## default print instructions
echo "  ############################"
echo "  ## NFS based installation ##"
echo "  ############################"
echo ""
echo "  Step 1:"
echo "  ======="
echo "    Setup the NFS server to share the CNV-WebStore base directory."
echo "    mandatory options: rw"
echo "    suggested options: async, no_subtree_check (for performance)"
echo ""
echo "  Step 2:"
echo "  ======="
echo "    Setup autofs and nfs-client on this compute node"
echo "    Get the mount location from the SCRIPTDIR variable on the nfs server .credentials file"
echo "    /etc/auto.master :   /-      /etc/auto.nfs"
echo "    /etc/auto.nfs    :   /mount_point	nfs-server:/source_point"
echo "      => eg: /opt/CNV-WebStore	143.169.2.1:/opt/CNV-WebStore" 
echo "      => IP-based approach does not suffer from DNS timeouts."
echo ""
echo ""
read -p "If these steps are performed, and the CNVanalysis-TMP database was created, all should work" DUMMY
exit	



