#!/usr/bin/perl
use DBI;
$|++;


################################### 
# CONNECT TO CNVanalysis DATABASE #
###################################
my $file = $ARGV[0];
if ($file eq '') {
	print "Path to credentials file not provided.\n";
	print " USAGE: perl ApplyUpdates.pl /path/to/.dbpass.txt\n";
	exit();
}
# read in config file
my %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
$userid = $config{'DBUSER'};
$userpass = $config{'DBPASS'};
$host = $config{'DBHOST'};
#print "Connecting to database\n";
#$mysql = DBI->connect("dbi:mysql:mysql:$host",$userid,$userpass) or die("Could not connect to database with credentials provided in $file");
#$mysql->{mysql_auto_reconnect} = 1;

# connect to GenomicBuilds DB to get the current Genomic Build Database. 
my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "CNVanalysis".$row[0];
$newbuild = $row[1];
#print "\t=> Using current build database (build ".$row[1].")\n";	
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
$uid = 1;

#################
# Read projects #
#################
print "\tReading projects\n";
my %projects;
open IN, "Database/control.project.txt";
my $head = <IN>;
my %chipcontrol = ();
my $sth = $dbh->prepare("INSERT INTO project (naam, chiptype, created, collection, userID, chiptypeid) VALUES (?,?,?,?,?,?)");
while (<IN>) {
	chomp($_);
	my @p = split(/\t/,$_);
	$sth->execute($p[1],$p[2],$p[3],$p[4],$p[5],$p[6]);
	my $id = $dbh->last_insert_id(undef, undef, undef, undef);
	$projects{$p[0]} = $id;
	$chipcontrol{$p[6]} = $chipcontrol{$p[6]}  . "$id,";
}
close IN;
$sth->finish();
unlink ('Database/control.project.txt');
#########################
# update chipcontroles ##
#########################
print "\tUpdating the controle projects for each chiptype:\n";
foreach (keys(%chipcontrol)) {
	my $chiptype = $_;
	my @pids = split(/,/,$chipcontrol{$chiptype});
	my %tmp = ();
	foreach (@pids) {
		$tmp{$_} = 1;
	}
	my $controlstring = join(',',sort(keys(%tmp)));
	$dbh->do("UPDATE chiptypes SET Controles = '$controlstring' WHERE ID = '$chiptype'");
}
	

################
# READ samples #
################
print "\tReading samples\n";
my %samples;
open IN, "Database/control.samples.txt";
$head = <IN>;
my $sth = $dbh->prepare("INSERT INTO sample (chip_dnanr, gender, intrack, trackfromproject, clinical) VALUES (?,?,?,?,?)");
while (<IN>) {
	chomp($_);
	my @p = split(/\t/,$_);
	if (exists($projects{$p[4]})) {
		$p[4] = $projects{$p[4]};
	}
	else {
		$p[3] = 0;
	}
	$sth->execute($p[1],$p[2],$p[3],$p[4],$p[5]);
	my $id = $dbh->last_insert_id(undef, undef, undef, undef);
	$samples{$p[0]} = $id;
}
close IN;
$sth->finish();
unlink("Database/control.samples.txt");

################################
## couple samples to projects ##
################################
print "\tReading project-sample details\n";
open IN, "Database/control.projsamp.txt";
$head = <IN>;
my $sth = $dbh->prepare("INSERT INTO projsamp (idsamp, idproj, callrate, idx, LRRSD, BAFSD, WAVE, calc, INIWAVE) VALUES (?,?,?,?,?,?,?,?,?)");
while (<IN>) {
	chomp($_);
	my @p = split(/\t/,$_);
	$p[1] = $projects{$p[1]};
	$p[0] = $samples{$p[0]};
	$sth->execute($p[0],$p[1],$p[2],$p[3],$p[4],$p[5],$p[6],$p[7],$p[8]);
}
close IN;
$sth->finish();
unlink("Database/control.projsamp.txt");
####################################
## Reading aberration information ##
####################################
my %aids;
print "\tReading aberration details\n";
open IN, "Database/control.aberration.txt";
$head = <IN>;
my $sth = $dbh->prepare("INSERT INTO aberration (cn, start, stop, chr, largestart, largestop, sample, idproj, inheritance, class, seenby, nrsnps, seeninchip, seenincontrols, schipacoll, size, seenall, nrgenes, avgLogR, pubmed, LiftedFrom) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
while (<IN>) {
	chomp($_);
	my @p = split(/\t/,$_);
	$p[8] = $projects{$p[8]};
	$p[7] = $samples{$p[7]};
	$sth->execute($p[1],$p[2],$p[3],$p[4],$p[5],$p[6],$p[7],$p[8],$p[9],$p[10],$p[11],$p[12],$p[13],$p[14],$p[15],$p[16],$p[17],$p[18],$p[19],$p[20],$p[21]);
	my $id = $dbh->last_insert_id(undef, undef, undef, undef);
	$aids{$p[0]} = $id;
}
close IN;
$sth->finish();
unlink("Database/control.aberration.txt");
#####################
## READ DATAPOINTS ##
#####################
print "\tReading probe level datapoints\n";
open IN, "Database/control.datapoints.txt";
$head = <IN>;
my $sth = $dbh->prepare("INSERT INTO datapoints (id, content, LiftedFrom) VALUES (?,?,?)");
while (<IN>) {
	chomp($_);
	my @p = split(/\t/,$_);
	$p[0] = $aids{$p[0]};
	$sth->execute($p[0],$p[1],$p[2]);
}
close IN;
$sth->finish();
unlink("Database/control.datapoints.txt");
## updating permissions, giving full access to user 1
print "\tSetting permissions\n";
while ( my ($key, $value) = each(%projects) ) {
    $dbh->do("INSERT INTO projectpermission (projectid, userid, editcnv, editclinic, editsample) VALUES ('$value', '1','1','1','1')");
}
print "\t => Control Data imported\n";
